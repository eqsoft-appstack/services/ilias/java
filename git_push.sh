IS_DIRTY=$(git status -s)

if [ -z "$IS_DIRTY" ] ; then
    echo "nothing to commit..."
    exit 0
fi

if [ $IS_LOCAL == 1 ] ; then
    if [ $FIRST_BUILD == 1 ] ; then
        git add VERSION
        git commit -m "[local build] Apply initial image build: $TARGET_IMAGE:$BASE_TAG"
        git push
    else
        git add --update
        git commit -m "[local build] Apply patch new version: $TARGET_IMAGE:$BASE_TAG"
        git push
    fi
else
    git config user.name "cibuilder"
    git config user.email "cibuilder@example.com"
    if [ $FIRST_BUILD == 1 ] ; then
        git add VERSION
        git commit -m "[ci build] Apply initial image build: $TARGET_IMAGE:$BASE_TAG"
        git push "https://cibuilder:${GITLAB_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git" "HEAD:${CI_COMMIT_BRANCH}"
    else
        git add --update
        git commit -m "[ci build] Apply patch new version: $TARGET_IMAGE:$BASE_TAG"
        git push "https://cibuilder:${GITLAB_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git" "HEAD:${CI_COMMIT_BRANCH}"
    fi
fi