# check version
# first build: no VERSION exists

FIRST_BUILD=0
DIGEST=
CURRENT_DIGEST=

if [ ! -f VERSION ] ; then
    FIRST_BUILD=1
    skopeo inspect --no-tags --format '{{ .Digest }}' docker://$BASE_IMAGE:$BASE_TAG | tr -d '[[:space:]]' > VERSION
fi

DIGEST=$(tr -d '[[:space:]]' < VERSION)
CURRENT_DIGEST=$(skopeo inspect --no-tags --format '{{ .Digest }}' docker://$BASE_IMAGE:$BASE_TAG)

if [ "$DIGEST" == "$CURRENT_DIGEST" ] && [ $FIRST_BUILD == 0 ] ; then
    if [ $FORCE == 1 ] ; then
        echo "no new digest for $BASE_IMAGE:$BASE_TAG ... forced build"
    else
        echo "no new digest for $BASE_IMAGE:$BASE_TAG ... nothing to do"
        exit 0
    fi
else
    echo -n $CURRENT_DIGEST > VERSION
    if [ $FIRST_BUILD == 1 ] ; then
        echo "first build..."
    fi
fi
